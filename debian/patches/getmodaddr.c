--- inn-1.7.2.orig/lib/getmodaddr.c
+++ inn-1.7.2/lib/getmodaddr.c
@@ -120,6 +120,7 @@
         (void)strcpy(GMApathname, _PATH_TEMPMODERATORS);
         (void)mktemp(GMApathname);
         GMAfp = GMA_listopen(GMApathname, FromServer, ToServer, "moderators");
+	if (GMAfp == NULL) GMAfp = fopen(_PATH_MODERATORS, "r");
     }
 
     if (GMAfp != NULL) {
--- inn-1.7.2.orig/doc/inews.1
+++ inn-1.7.2/doc/inews.1
@@ -129,7 +129,8 @@
 If an unapproved posting is made to a moderated newsgroup,
 .I inews
 will try to mail the article to the moderator for posting.
-It uses the
+It will query the remote news server for a moderators listing. If
+that doesn't succeed, it will fallback to using the local
 .IR moderators (5)
 file to determine the mailing address.
 If no address is found, it will use the
