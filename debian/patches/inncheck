--- a/samples/inncheck
+++ b/samples/inncheck
@@ -99,16 +99,16 @@ $newsgroup = 'news';
 ##  The modes of the config files we can check.
 %modes = (
     'active',		0644,
-    'control.ctl',	0440,
-    'expire.ctl',	0440,
-    'hosts.nntp',	0440,
-    'inn.conf',		0444,
-    'moderators',	0444,
-    'newsfeeds',	0444,
-    'overview.fmt',	0444,
-    'nnrp.access',	0440,
-    'nntpsend.ctl',	0440,
-    'passwd.nntp',	0440
+    'control.ctl',	0644,
+    'expire.ctl',	0644,
+    'hosts.nntp',	0640,
+    'inn.conf',		0644,
+    'moderators',	0644,
+    'newsfeeds',	0644,
+    'overview.fmt',	0644,
+    'nnrp.access',	0640,
+    'nntpsend.ctl',	0644,
+    'passwd.nntp',	0640
 );
 
 
@@ -743,6 +743,22 @@ checkperm
     die "Internal error, undefined mode in perm from ", (caller(0))[2], "\n"
 	if !defined $m;
 
+    # system files are usually owned by root (or news if suid or sgid)
+    if ($f !~ m#^/(?:run|var)/#) {
+	$u = 'root';
+	$g = $m & 06000 ? 'news' : 'root';
+    }
+    # if they are not world readable then they must be owned by group news
+    if ($f =~ m#^/etc/#) {
+	$g = $m & 004 ? 'root' : $newsgroup;
+    }
+
+    # binaries should be 755 (or x744 if suid or sgid)
+    if ($m & 0100 and not -d $f) {
+	$m |= $m & 06000 ? 0744 : 0755;
+    }
+
+
     if ( ! -e $f ) {
 	print "$pfx$f:0: missing\n";
     }
@@ -804,7 +820,7 @@ intersect
 }
 
 @directories = (
-    'archive', 'badnews', 'batchdir', 'ctlprogs', 'most_logs', 'newsbin',
+    'archive', 'badnews', 'batchdir', 'ctlprogs', 'most_logs', # 'newsbin',
     'newslib', 'oldlogs', 'rnewsprogs', 'spooltemp', 'spool', 'spoolnews'
 );
 @control_scripts = (
@@ -812,7 +828,7 @@ intersect
     'sendme', 'sendsys', 'senduuname', 'version'
 );
 @rnews_programs = (
-    'c7unbatch', 'decode', 'encode'
+    'decode', 'encode', 'gunbatch', 'xunbatch'
 );
 @newsbin_public = (
     'archive', 'batcher', 'buffchan', 'convdate', 'cvtbatch', 'expire',
@@ -822,24 +838,23 @@ intersect
 );
 @newsbin_private = (
     'ctlinnd', 'expirerm', 'inncheck', 'innstat', 'innwatch',
-    'makegroup', 'news.daily', 'nntpsend', 'scanlogs', 'sendbatch',
-    'tally.control', 'tally.unwanted', 'updatemods', 'writelog'
+    'news.daily', 'nntpsend', 'scanlogs', 'sendbatch',
+    'tally.control', 'tally.unwanted', 'writelog'
 );
 @newslib_private = (
     'send-ihave', 'send-nntp', 'send-uucp'
 );
 @newslib_private_read = (
-    'innlog.pl'
 );
 
 ## The modes for the various programs.
 %prog_modes = (
-    'inews',		02555,
-    'innd',		 0555,
-    'newsboot',		 0550,
-    'nnrpd',		 0555,
-    'parsectl',		 0550,
-    'rnews',		02555
+    'inews',		02755,
+    'innd',		 0755,
+    'newsboot',		 0755,
+    'nnrpd',		 0755,
+    'parsectl',		 0755,
+    'rnews',		02755
 );
 
 ##  Check the permissions of nearly every file in an INN installation.
@@ -852,37 +867,37 @@ check_all_perms
     local ($newslib) = $paths{'newslib'};
 
     foreach ( @directories ) {
-	&checkperm($paths{$_}, 0775);
+	&checkperm($paths{$_}, $_ =~ /ctlprogs|oldlogs|rnewsprogs/ ? 0755 : 0775);
     }
-    &checkperm($paths{'innddir'}, 0770);
+    &checkperm($paths{'innddir'}, 0755);
     foreach ( keys %prog_modes ) {
-	&checkperm($paths{$_}, $prog_modes{$_});
+	&checkperm($paths{$_}, $prog_modes{$_}, 'root');
     }
-    &checkperm($paths{'inndstart'}, 0555, 'root', 'bin');
+    &checkperm($paths{'inndstart'}, 04754, 'root', 'news');
     foreach ( keys %paths ) {
 	&checkperm($paths{$_}, $modes{$_})
 	    if defined $modes{$_};
     }
-    &checkperm($paths{'history'}, 0644);
-    &checkperm($paths{'history'} . ".dir", 0644);
-    &checkperm($paths{'history'} . ".pag", 0644);
+    &checkperm($paths{'history'}, 0664);
+    &checkperm($paths{'history'} . ".dir", 0664);
+    &checkperm($paths{'history'} . ".pag", 0664);
     foreach ( @newslib_private ) {
-	&checkperm("$newslib/$_", 0550);
+	&checkperm("/etc/news/scripts/$_", 0755);
     }
     foreach ( @newslib_private_read ) {
-	&checkperm("$newslib/$_", 0440);
+	&checkperm("$newsbin/$_", 0644, 'root', 'root');
     }
     foreach ( @newsbin_private ) {
-	&checkperm("$newsbin/$_", 0550);
+	&checkperm("$newsbin/$_", 0755, 'root', 'root');
     }
     foreach ( @newsbin_public ) {
-	&checkperm("$newsbin/$_", 0555);
+	&checkperm("$newsbin/$_", 0755, 'root', 'root');
     }
     foreach ( @control_scripts ) {
-	&checkperm("$ctlprogs/$_", 0550);
+	&checkperm("$ctlprogs/$_", 0755);
     }
     foreach ( @rnews_programs ) {
-	&checkperm("$rnewsprogs/$_", 0555);
+	&checkperm("$rnewsprogs/$_", 0755);
     }
 
     ##  Also make sure that @rnews_programs are the *only* programs in there;
@@ -999,8 +1014,15 @@ action: foreach $workfile ( @todo ) {
 	print "$pfx$workfile:0: can't open $!\n";
 	next action;
     }
-    &checkperm($file, $modes{$workfile})
-	if $noperms == 0 && !$perms && defined $modes{$workfile};
+	if ($noperms == 0 && !$perms && defined $modes{$workfile}) {
+	if ($workfile eq 'active') {
+    &checkperm($file, $modes{$workfile});
+	} elsif (grep($workfile eq $_, qw(passwd.nntp nnrp.access hosts.nntp))) {
+    &checkperm($file, $modes{$workfile}, 'root', 'news');
+	} else {
+    &checkperm($file, $modes{$workfile}, 'root', 'root');
+	}
+	}
     $line = 0;
     eval "&$checklist{$workfile}" || warn "$@";
     close(IN);
