--- inn-1.7.2.orig/include/paths.h
+++ inn-1.7.2/include/paths.h
@@ -177,7 +177,14 @@
 #define _CONF_CONTENTTYPE	"mime-contenttype"
     /* Default encoding */
 #define _CONF_ENCODING		"mime-encoding"
-
+    /* Default listening address */
+#define _CONF_BINDADDRESS	"bindaddress"
+    /* Default NNTP port */
+#define _CONF_PORT		"port"
+    /* Try using actived */
+#define _CONF_USEACTIVED	"useactived"
+    /* Disable internal control messages processing */
+#define _CONF_USECONTROLCHAN	"usecontrolchan"
 
 /*
 **  13.  TCL Support
