When posting an article with inews, first the active file is gotten from
the server. Then inews tries to read the distrib.pats file from the server
using CA_listopen.  If that _fails_ (no remote copy and no local copy) CAclose
is called by CA_listopen, and CApathname is removed and CAfp closed. Ofcourse
those static variables are references to the active file opened with CAopen()..

The following patch fixes this, but IMO CApathname and CAfp should not
be used at all.

Miquel van Smoorenburg  <miquels@cistron.nl>, 23-Mar-1998

--- inn-1.7.2/lib/clientactive.c.orig	Mon Mar 23 17:15:54 1998
+++ inn-1.7.2/lib/clientactive.c	Mon Mar 23 17:15:56 1998
@@ -67,7 +67,8 @@
     if (fgets(buff, sizeof buff, FromServer) == NULL
      || !EQn(buff, NNTP_LIST_FOLLOWS, STRLEN(NNTP_LIST_FOLLOWS))) {
 	oerrno = errno;
-	CAclose();
+	/* Only call CAclose if opened through CAopen() */
+	if (strcmp(CApathname, pathname) == 0) CAclose();
 	errno = oerrno;
 	return NULL;
     }
