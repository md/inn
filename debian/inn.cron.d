PATH=/usr/lib/news/bin:/bin:/sbin:/usr/bin:/usr/sbin
MAILTO=usenet

# Expire old news and old overview entries nightly, generate reports.
08 4	* * *	news	[ -x /usr/lib/news/bin/news.daily ] && /usr/lib/news/bin/news.daily expireover delayrm

# Refresh the hosts cache every night.
02 4	* * *	news	[ -x /usr/lib/news/bin/ctlinnd ] && /usr/lib/news/bin/ctlinnd -t 300 -s reload hosts.nntp "flush cache"

# Every hour, run an rnews -U. This is not only for UUCP sites, but
# also to process queud up articles put there by in.nnrpd in case
# innd wasn't accepting any articles.
14 *	* * *	news	[ -x /usr/bin/rnews ] && /usr/bin/rnews -U

# Enable this entry to send posted news back to your upstream provider.
# Not if you use innfeed, ofcourse.
#*/15 *	* * *	news	send-nntp news.myprovider.com

# Enable this if you want to send news by uucp to your provider.
# Also edit /etc/news/send-uucp.cf !
#22 *	* * *	news	send-uucp.pl
