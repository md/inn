/*
 * $Revision: 1.22 $
 */


/*
**  If you make local modifications to INN, change this line as needed:
*/
#define LOCAL_STRING	""


/*
**  Try to avoid changing these.
*/
#define RELEASE "1"
#define PATCHLEVEL "7.2"
#define DATE "08-Dec-1997"


/*
**  See lib/innvers.c, but the version string should look like this:
**	INN ${RELEASE}.${PATCHLEVEL} ${DATE} (${LOCAL_STRING})
**  for example:
**	INN 1.0 10-Feb-92 (FOO.COM)
*/
#define RELEASE_C 1
#define PATCHLEVEL_C 7
#define SUB_PATCHLEVEL_C 1
