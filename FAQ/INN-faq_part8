Newsgroups: news.software.nntp,news.software.b,news.answers
Approved: hwr@pilhuhn.de
Subject: INN FAQ Part 8/9: Appendix A: Norman's install guide
Followup-To: news.software.nntp
Summary: This article is part 8 of a multi-part FAQ:
    Part 8: Norman's quick guide to getting started (assumes SunOS and other things), and misc. other things.

Posted-By: post_faq 2.10
Archive-name: usenet/software/inn-faq/part8
Last Changed: $Date: 1996/01/30 21:44:45 $ $Revision: 2.3 $

		Part 8 of 9

INN FAQ Part 1: General and questions from people that don't (yet) run INN
INN FAQ Part 2: Specific notes for specific operating systems
INN FAQ Part 3: Reasons why INN isn't starting
INN FAQ Part 4: The debugging tutorial (setup of feeds etc.)
INN FAQ Part 5: Other error messages and what they mean
INN FAQ Part 6: Day-to-day operation and changes to the system
INN FAQ Part 7: Problems with INN already running
INN FAQ Part 8: Appendix A: Norman's install guide
INN FAQ Part 9: Appendix B: Configurations for certain systems


This is a separate guide for installing INN addressed to 
UNIX and System Administration novices.  It is written for 
installation of INN 1.4 on a Sun SPARCstation running 
SunOS 4.1.x with some additional comments
for Solaris 2.x.  This guide is maintained by 
Norman J. Pieniazek (norman@giardia.pdb.cdc.gov).  Please 
send updates and corrections to him.

This guide is composed of following parts:

  I. SYSTEM REQUIREMENTS
 II. INSTALLATION OF INN 1.4
III. UPGRADING FROM INN 1.3 TO INN 1.4
 IV. MAINTENANCE OF INN
  V. INSTALLATION OF XVNEWS, A SUN OPENWINDOWS
     NEWSREADER



I. SYSTEM REQUIREMENTS

  1. INN will not install nor run on Sun SPARCstation under
     plain Solaris 1.1  or 2.x unless you install some
     additional software packages and utilities
     (fortunately, they are free).  Also, your machine's IP
     number (of the news server) has to be entered in a
     Domain Name Service database (DNS) to be resolvable.
     Ask your Network Administrator about DNS or read the
     O'Reilly & Associates, Inc. book: "DNS and BIND" by Paul
     Albitz & Cricket Liu, ISBN 1-56592-010-4.
     
     Further, test the resolver on your machine by
     pinging a hostname not entered in your /etc/hosts file.
     For example, type: 
      "ping ftp.uu.net"
     The response should be: "ftp.uu.net is alive".

     If you get: "unknown host ftp.uu.net", consult the
     O'Reilly's book.

     (Solaris 2.x - the most common problem with DNS 
     results from not switching on DNS support.  Check
     the line "hosts:" in the /etc/nsswitch.conf file.
     It should read: "hosts:   files dns").
   
  2. Be sure to su to root before performing tasks
     outlined below.  Next, check if you have a directory
     /usr/local and about 90 Mb of space in the partition
     where this directory is located.  Create directories:
     /usr/local/bin, /usr/local/lib and /usr/local/man if
     you do not have them.  Add /usr/local/bin to your path
     and /usr/local/man to your MANPATH.  On Solaris 2.x,
     create /opt/local tree and make a symbolic link
     from /usr ("ln -s /opt/local /usr/local").
         
  3. INN uses gnu versions of grep, awk and sed.  Moreover,
     it is  recommended to compile INN with the gnu
     versions of make and the gnu gcc compiler instead of
     the Sun's Solaris 1.x unbundled cc compiler. (C
     compiler is not included with Solaris 2.x.  The cc in
     /usr/ucb is bad.  Be sure to remove it from path.)  If you
     don't have the gnu software package, create a directory
     /usr/local/gnu, cd to this directory and ftp to
     prep.ai.mit.edu (18.71.0.38). Go to directory /pub/gnu
     and get the newest version of the following programs:

      bison      bison-x.x.tar.gz    
      flex       flex-x.x.x.tar.gz 
      gawk       gawk-x.x.x.tar.gz
      gcc        gcc-x.x.x.tar.gz
      grep       grep-x.x.tar.gz
      gzip       gzip-x.x.x.tar (Note: no .gz)
      make       make-x.x.tar.gz
      patch	 patch-x.x.tar.gz
      perl	 perl-x.x.tar.gz
      sed        sed-x.x.tar.gz
      tar        tar-x.x.x.tar.gz
      texinfo    texinfo-x.x.tar.gz

     Installation of these programs is not complicated,
     and is really beneficial both for Solaris 1.x and
     Solaris 2.x users.  After you complete the
     installation, check if older versions of these programs
     are not present somewhere in your path.  Delete or
     rename these old versions.

     Solaris 2.x: First, after connecting through ftp
     to prep.ai.mit.edu, cd to directory:
       /pub/gnu/sparc-sun-solaris2
     and get the file INSTALL.gcc.  Follow the instructions
     for installing binaries for gcc and gzip (installed
     in /opt/gnu).  Next, follow the instructions for
     Solaris 1.1, but read the doc file for gcc-x.x.x.
     To avoid problems, rename /usr/ucb/cc to something 
     else, or remove it from path.  Make a symbolic link
     in the /opt/gnu/bin directory from gcc to cc.    
	When you finish the installation, the new utilities
     will be installed in the /usr/local tree.  Delete and
     remove from path the /opt/gnu tree.  As the binaries
     you downloaded are pretty old, it is advisable to
     start with the new gcc and to compile all the
     gnu utilities with this new gcc.  NOTE: it makes
     no difference, if you install gnu stuff in /opt/gnu
     or in /usr/local.  The installation scripts for gnu
     utilities default to the /usr/local directory tree,
     so I find it simple to use this path.  Whatever
     you do, be sure to have the gnu utilities in
     your path before /usr/bin and /usr/ccs/bin.

     Solaris 2.x: be sure to include /usr/ccs/bin
     and /usr/ccs/lib in your path.  Important
     utilities (e.g. - ar, cpp) are located there.
     Please note, that the /usr/ccs stuff is only
     installed, when you do a full install of Solaris.
     If you do not have e.g. - /usr/ccs/bin/ar, re-install
     Solaris 2.x!

   a. Start with gzip.  In /usr/local/gnu type:
      "tar xvf gzip-x.x.x.tar".  A directory gzip-x.x.x will
      be created.  Change to this directory and type:
      "sh ./configure".  Next, edit the Makefile
      with the vi editor and change the line starting with
      "CC" to read: "CC = cc". Then, type: "make".
      Compilation should complete without errors.  Finally,
      type: "make install" to complete installation of gzip,
      gunzip, and other utilities in /usr/local/bin.     
  
   b. Compile and install texinfo - info, makeinfo, the GNU
      hypertext system.  Makeinfo is needed for error-free
      installation of many GNU programs and utilities.
      In /usr/local/gnu type: "gunzip texinfo-x.x.tar.gz".
      Next, type: "tar xvf texinfo-x.x.tar".  A directory
      texinfo-x.x will be created.  Cd to this directory
      and type: "sh ./configure".  Next, edit the Makefile
      as described above for gzip.  Now, type: "make"
      and ignore any non-fatal errors.  Finally, type:
      "make install".  With newer versions of texinfo,
      you will get a lot of messages about "emacs",
      "*.elc", and "*.el" files.  Just ignore these
      messages.
 
   c. Installation of the gnu version of tar is
      beneficial.  You will be able to use
      "tar -Zxvf <file name>.tar.Z" or
      "tar -zxvf <file name>.tar.gz" to uncompress
      and untar files in one step.  Uncompress, compile
      and install tar as above.  Be sure that /usr/local/bin
      is the first directory in your path.

   d. Uncompress, compile and install gnu make.  Follow the
      steps described for texinfo.  Gnu make will install in
      /usr/local/bin.  To disable Sun's make, cd to /usr/bin
      (/usr/ccs/bin for Solaris 2.x) and type:
       
       "mv make make.sun".

   e. Compile and install bison following the procedure
      described for texinfo.

   f. Uncompress gcc.  Cd to /usr/local/gnu/gcc-x.x.x and
      type:

         sh ./configure

      Next type:
        make
      This step will take about 17 minutes to complete.
      Ignore warnings reported for insn-emit.c.  Next, type:
        make stage1
      Ignore reported non-fatal errors.  Next, type:
        make CC="stage1/xgcc -Bstage1/" CFLAGS="-g -O"
      This step will take about 19 minutes to complete.
      Next, type:
        make stage2
      Ignore reported non-fatal errors.  Next, type:
        make CC="stage2/xgcc -Bstage2/" CFLAGS="-g -O"
      In this step, which lasts about 13 minutes, you will
      make the final stage 3 compiler.  Now type: "make
      compare". No errors should be reported here.  Finally,
      type:
        make install CC="stage2/xgcc -Bstage2/" CFLAGS="-g\
        -O"
      (NOTE: backslash "\" is used in this FAQ to break the
      line for formatting purposes.  You can use the 
      backslash for the same purpose in a command line.
      Please read Sun documentation, if you need more
      information.) 
      to install the gcc compiler.  It is advisable to
      recompile patch, gzip, texinfo, make and bison with
      gcc.  It will be a good test for proper gcc
      functionality.

   g. Uncompress gawk. Cd to directory gawk-x.x.x and type:
       sh ./configure sunos41
        (for Solaris 2.x: sh ./configure solaris2.cc)     
	Next type: "make".  When gawk will compile, type:
        make install
      Finally, disable Sun's awk by typing:
        mv /usr/bin/awk /usr/bin/awk_sun
      and create a link to gawk for awk by typing:
        ln -s /usr/local/bin/gawk /usr/bin/awk

   h. Uncompress grep.  Cd to directory grep-x.x and type:
        ./configure
      Next type: "make" and "make install".  Grep will
      also be installed in /usr/local/bin as fgrep
      and egrep.  Finally, as some programs expect to find
      grep, egrep and fgrep in /usr/bin, disable Sun's grep
      and create appropriate links, just like described
      for awk.  INN expects to find a program "egnugrep" in
      /usr/local/bin.  Create it by typing:
         cp /usr/local/bin/egrep /usr/local/bin/egnugrep

   i. Uncompress sed and patch.  Compile and install these
      utilities following the instructions for grep.


   j. INN comes with some scripts written in the perl
      language.  Uncompress perl, cd to directory perl-x.xxx
      and type:
       sh ./Configure
      and hit Return for default answers to all questions.
      Next, type: "make depend", then "make".  Now type:
       make test
      to check, if perl works without errors.  Finally,
      type:
       make install
      to complete installation of perl.
      
      Solaris 2.x: answer "none" for -I/usr/ucbinclude
      and add libraries: -lm -lnsl -lsocket

II. INSTALLATION OF INN 1.4


  1. As root, create directories: /usr/local/inn1.4 (this
     will be your $inn directory) and /usr/local/news.
     (Solaris 2.x, you have to create a user "news",
     good choice is UID=6, GID=13, home
     directory=/var/spool/news, shell /bin/csh, no password
     - account is locked, * in password field in /etc/shadow.
     Be sure, to create a new group in /etc/group "news"
     with GID=13.)

  2. Cd to $inn. Get the newest version of inn from:
     ftp.uu.net (192.48.96.9).  This file is in the
     directory: /networking/news/nntp/inn.   As for all
     binary files, please remember to set the file type in
     ftp to binary.

  3. From the same source and directory get the Frequently
     Asked Questions (FAQ) files:
       faq-inn-1.Z
       faq-inn-2.Z
       faq-inn-3.Z
       faq-inn-4.Z (this file)
       faq-nov.Z
     and any other files that may contain patches or 
     information.

  4. To uncompress the FAQ files type: "uncompress faq-*.Z.
     Print the FAQs and read them before proceeding further.

  6. In $inn type (with gnu tar):
     "tar -Zxvf innx.x.tar.Z
     This will extract inn files for compilation.

  7. Change directory to $inn/config, and type:
       cp config.dist config.data
       chmod 644 config.data
     Now use the vi editor to edit config.data.  You should
     select gcc as the compiler by changing the line that
     starts with "CC" to read "CC   gcc".  Many lines in
     config.data define the location of various files
     and directories.  It's OK to leave defaults, but
     check if you have ample space in the directory
     where the articles will be stored (/var/spool).  More
     information on where INN expects to find certain files
     is in: $inn/samples/innshellvars.  If necessary, edit
     the config.data and innshellvars to reflect your
     configuration.  Please make sure that specified
     directories and utilities exist on your system.
  
  
  8. Change directory to $inn.  Type: "make world".  Now
     type:
       cat */lint
     (Solaris 2.x - there is not lint included with
     Solaris 2.x.  One way to get Solaris 2.x version
     of lint suggested to me, is to get a 30 day trial
     package of the SunPro C compiler from Sun.  However,
     lint is not really needed, except for testing new
     code.  You can make a symbolic link from /bin/echo
     to lint.)
     lint will give you a list of compiler warnings and
     errors.  In addition, "make" creates in this step
     a file:  "$inn/Install.ms".  Print it by typing:
       nroff -ms Install.ms | lpr
     Install.ms is a manual for installation of INN written
     by Rich Salz, the author of INN.  There is a lot of
     important and interesting information in his manual.

  9. If you got no fatal errors from make, go to the
     $inn/site directory
     and type: "make all".

 10. There are some scripts and control files listed in
     inn-faqs and Install.ms that may be modified at this
     point.  For most installations, the default settings
     are OK.

 11. Now cd to $inn and type: "make install" to complete
     basic installation of INN.

 12. To finish installation of INN, run the BUILD script
     in the $inn directory by typing "sh BUILD".  This
     script will ask a series of configuration questions
     that are easy to answer.  Please note that if your have
     "history.*" files in your /usr/local/news directory
     from a previous or failed installation of INN, the
     BUILD script will not complete.  Rename them to
     "old.history.*" and re-run the BUILD script.

 13. You need a site that will feed news to you.
     This will depend on your geographic location and
     organization.  Ask a System Administrator of a site
     close to you for hints.

 14. Ftp to your newsfeeding site and get the "active" file
     from there.  Place this file in your /usr/local/news
     directory and edit it to your taste.  Remember to
     include the "control" and "junk" newsgroups.  Also, you
     have to edit the following files in /usr/local/news:
     newsfeeds, hosts.nntp, nnrp.access, inn.conf.
     Be careful with editing the "active" file
     with vi or textedit.  A common error is to leave a
     blank line at the top.  This error causes INN to
     crash with the message: "ME bad_active".  Depending
     on your setup, you will get this message in the syslog
     or on the console.

 15. To check for syntax errors in INN control files,
     file ownership, permissions and other things type:
       /usr/local/bin/perl /usr/local/news/bin/inncheck
     and correct any errors reported by inncheck.
     If your paths are set, it is sufficient to
     change to the /usr/local/news/bin directory and type:
       "perl ./inncheck" or "./inncheck".

 16. Look at the file "rc.news" in /usr/local/etc.
     It's advisable to enable the innwatch utility.
     Innwatch will throttle the newsserver when your disk
     will get full and prevent crashes.  Change the line
     starting with "DOINNWATCH" to read "DOINNWATCH=true".
     (Solaris 2.x: edit the innwatch.ctl file and replace
     df with /usr/ucb/df.  Default Solaris 2.x
     /usr/sbin/df doesn't understand the -i option.)
     Now type: "sh /usr/local/etc/rc.news"
     and look in /var/log/syslog for errors.  Also, type:
       ps -aux| grep news
     and check, if the innd process owner is news.
     
     On Solaris 2.x, the syntax is a bit different:
       ps -fu news

 17. In /etc/aliases create an entry: "usenet: <you, or
     root>".  Next type: "/usr/ucb/newaliases" to inform the
     sendmail program that the aliases file has changed.
     If you are running YP (NIS) on your network, you may
     optionally add the "usenet" alias to your YP aliases
     file.  If the machine you are installing INN on is the
     YP master and the file "/etc/aliases" is the source of
     the YP aliases map, you should type: "cd /var/yp"
     followed by "make".  Reboot and restart innd (see #16). 
   

 18. To get daily reports on the newsserver activity from
     the "news.daily" script and to enable the "expire"
     utility type: "/bin/crontab -e news" and insert the
     following line:
       40 23 * * * /usr/local/news/bin/news.daily delayrm
     or, if you are using overchan (in newsfeeds):
       40 23 * * * /usr/local/news/bin/news.daily delayrm\
       expireover

 19. To set-up complete logging of the server activities
     and have them ready for a report by the "news.daily"
     script, you have to edit your /etc/syslog.conf file.
     Insert at the end of this file the content of
     the syslog.conf file written by Rich Salz.  This file
     is in: $inn/syslog.  Please check, if you have the
     directories and files mentioned in Rich's syslog.conf.

 20. Run tests from your machine to the server (to your
     machine at telnet port 119).  See inn-faqs for details.

 21. After completing these test, be sure to delete the
     entry for your machine from the hosts.nntp file.  If
     you will not do it, your machine will be treated as a
     "feeder" and not as a "reader".

 22. Set posting. 

      a. Edit the /usr/local/news/newsfeeds file and add:
           <alias for your feed>/<full address of feed>\
           :*\ (for all local postings)
           :Tf,Wnm: (standard entry)

      b. Edit /usr/local/news/nntpsend.ctl file and add:
           <alias for your feed>:<full address of feed>::\
           -T1800 -t300

      c. Type: "/bin/crontab -e news" and insert a line:
           0,10,20,30,40,50 * * * *\
            /usr/local/news/bin/nntpsend
  
      d. Run inncheck (see #15).

      e. Post to misc.test and include "reply" in the
         Subject line, automatic responses will be mailed to
         usenet (see #17) within a few minutes.

 23. To start innd automatically at bootup, include at the
     end of your rc.local the following lines:

      #
      # Start INN news service - Internet News Daemon (innd)
      #
      if [ -f /usr/local/etc/rc.news ]; then
         /usr/local/etc/rc.news; echo "Starting INN news
				 service"
      fi
     
	(In Solaris 2.x there is no rc.local.  If you want
     to start INN automatically, you should create a
     script in /etc/rc2.d).

     NOTE: Please send me your solutions for boot time
     starting and shutdown closing of INN on Solaris!

 24. Watch the news.daily reports in your mail for any
     additional errors.  To run the news.daily script
     at any time manually, first su from root to "news"
     and type: "/usr/local/news/news.daily".  A mail to
     "usenet" should arrive within a couple of minutes
     and may report important configuration problems.


III. UPGRADING FROM INN 1.3 TO INN 1.4

  1. Stop the server.  Type:
       ctlinnd shutdown "upgrade"

  2. Create a directory /usr/local/inn1.4, this will be
     your $inn directory.  cd to $inn.  Next, ftp to
     ftp.uu.net, cd to directory: /networking/news/nntp/inn
     and get the following files:

        inn1.4sec.tar.Z
        faq-inn-1.Z
        faq-inn-2.Z
        faq-inn-3.Z
        faq-inn-4.Z (this file)
        faq-nov.Z
     Uncompress and print the FAQs.  Next, type
     (with gnu tar):
        tar -Zxvf inn1.4sec.tar.Z

     Print the README file.  To print the Install file,
     type: "make Install.ms" followed by:
        nroff -ms Install.ms | lpr

  3. Now, cd to $inn/config and type the following commands:
        make subst
        cp config.dist config.data
        ./subst -f {OLDFILE} config.data
     where {OLDFILE} is the location of config.data in your
     INN 1.3 directory.  Be sure to check, if in your old
     config.data the "CC" line reads "CC  gcc".
     Ignore warnings about new lines.  Next, type:
        make sedtest
     No errors should be reported here.  Now, cd to $inn
     and type this series of commands:
        make quiet
        cd ../lib
        make libinn.a lint
        cd ../frontends
        make all
        cd ../innd
        make all
        cd ../nnrpd
        make all
        cd ../backends
        make all
        cd ../expire
        make all
        cd ../site
        make all
        cd ..
        make update

  4. Restart innd.


IV. MAINTENANCE OF INN

 1. Adding new groups - see also Part IV, Section 3.b,:

     a. Type: "ctlinnd pause 'edit active'"

     b. Edit the active file.  The format is:
          groupname himark lomark flag.
        Set himark to 0000000000 and lomark to 0000000001.
        For detailed description of flags, read the man
        page for active.  Editing the active file by hand
        is easy, but will not update the information in
        that file active.times that some newsreaders
        require to trace new newsgroups.  Using ctlinnd
        command "newgroup" takes care of this problem
        (see below).  Editing the active file with an
        editor has one drawback - the file active.times
        is not automatically updated.  This creates problems
        for some newsreader (Trumpet being one of them) 
        that use this file to alert users to new groups.
        Using ctlinnd command "newgroup" (see Part IV.3.b)
        corrects this problem. 

    c. Run inncheck (see #11) to check the new active file
       for errors. 
  
    d. Type: "ctlinnd reload active 'new active'".

    e. Type: "ctlinnd go 'edit active'".


 2. New groups are sometimes added automatically through
    a control message.  A mail message to usenet will alert
    to such an automatic change to the active file.  If you
    do not want to subscribe to a particular news group,
    change directory to /usr/local/news and type:
      ctlinnd rmgroup <group name>

 3. Sometimes, a mail message will arrive for usenet with
    a checkgroups file.  Remove header, save the body of
    the message in:
      /usr/local/news/bin/control/news_control/news_control_
					todaysdate
    cd to that directory and type:
      ../docheckgroups <news_control_todaysdate
			 >todaysdate_pre
    Read the output file (todaysdate_pre) and carry out all 
    the instructions that you think pertain to your
    situation.  Read the man pages for "active" and
    "ctlinnd" for more info.  To implement the changes, you
    will have to change to the directory: /usr/local/news
    and:  
    
     a. remove a discontinued group:
          type: "ctlinnd rmgroup <group name>"

     b. add a group:
        type: "ctlinnd newgroup <group name> flag <creator
                                           name>"

     c. mark a group correctly:
        type: "ctlinnd changegroup <group name> flag"

    Run inncheck and repeat the docheckgroups command from
    the /usr/local/bin/control/news_control directory
    by typing:
      ../docheckgroups <news_control_todaysdate
                                >todaysdate_after
    Correct any reported problems.  Also, you may have to
    edit the /usr/local/news/newsgroups file to reflect
    any changes you introduced.

 4. Read the reports mailed to usenet (you) by the
    news.daily script.  Most errors are reported here and
    should be easy to fix.

 5. Subscribe to and read the following newsgroups:
      news.software.nntp
      news.software.readers
      

 6. IMPORTANT!!! Never run fsck on the drive where the
    /spool/news files are located while running INN.  Innd
    has a lot of active disk I/O going on and you could
    corrupt your drive. Use ctlinnd to throttle, pause, or
    shutdown innd first - see the manual page for ctlinnd.


  V. INSTALLATION OF XVNEWS, A SUN OPENWINDOWS
     NEWSREADER

   1. Create a directory /usr/local/xvnews and cd to this
      directory.

   2. Ftp to ftp.twi.tudelft.nl and get the file
      xvnews.tar.gz from directory /pub/news.

   3. Ungzip and untar xvnews.  With gnu tar type:
        tar -zxvf xvnews.tar.gz

   4. Use your vi editor and edit the file xvnews.h
      You will need to enter your settings for:
        DOMAIN         (on my machine: "pdb.cdc.gov")
        ORGANIZATION   (on my machine: "Centers for Disease
                          Control Atlanta, GA, USA")
        NNTPSERVER     (on my machine:
                               "giardia.pdb.cdc.gov")
      Preferred method is to override these defaults
      by specifying environment variables DOMAIN, ORGANIZATION,
      and NNTPSERVER in your .cshrc or .profile files
      (see the xvnews man page).  In this way, you will be able
      to change default settings quickly.

   5. Compile xvnews by typing:
      "make -f Makefile.dist"
      This command will show you, how to enter defaults
      for your operating system (e.g. - "sunos5" for Solaris 2.x,
      "sunos4" for SunOS 4.1.x).  I get some compilations
      warnings on my system, but the program runs OK.  Copy xvnews
      to /usr/local/bin and xvnews.man to directory:
      /usr/local/man/man1 as xvnews.1.  



