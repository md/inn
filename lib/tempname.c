/* -*- c -*-
 *
 * Author:      James Brister <brister@vix.com> -- berkeley-unix --
 * Start Date:  Mon Jul 15 14:27:34 1996 +0200
 * Project:     INN
 * File:        tempName.c
 * RCSId:       $Id: tempname.c,v 1.2 1996/10/16 15:14:20 brister Exp $
 *
 * Copyright:	Copyright (c) 1996 by Internet Software Consortium
 *
 *              Permission to use, copy, modify, and distribute this
 *              software for any purpose with or without fee is hereby
 *              granted, provided that the above copyright notice and this
 *              permission notice appear in all copies.
 *
 *              THE SOFTWARE IS PROVIDED "AS IS" AND INTERNET SOFTWARE
 *              CONSORTIUM DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS
 *              SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *              MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL INTERNET
 *              SOFTWARE CONSORTIUM BE LIABLE FOR ANY SPECIAL, DIRECT,
 *              INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *              WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 *              WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 *              TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE
 *              USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Description: Doug Needham's temp filename function.
 * 
 */

#if ! defined (lint)
static const char *rcsid = "$Id: tempname.c,v 1.2 1996/10/16 15:14:20 brister Exp $" ;
static void use_rcsid (const char *rid) {   /* Never called */
  use_rcsid (rcsid) ; use_rcsid (rid) ;
}
#endif


#include <stdio.h>
#include <sys/types.h>
#include "configdata.h"
#include "clibrary.h"
#include <errno.h>


/*
**  Make a temporary filename that is unlikely to collide with mktemp
**  or cause problems for sites with 14-character filename limits.
*/
void
TempName(dir, buff)
    char	*dir;
    char	*buff;
{
    time_t	now;

    (void)time(&now);
    now &= 0xFFFFFFFF;
    (void)sprintf(buff, "%s/%08lxXXXXXX", dir, (long)now);
    (void)mktemp(buff);
}

