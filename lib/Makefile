##  $Revision: 1.37 $
SHELL	= /bin/sh
MAKE	= make
IFTRUE	= @$(SHELL) ../iftrue.sh
NOPATCH	= 'rm dbz.c ; echo Cannot apply patch ; exit 1'

##  =()<P	= @<P>@>()=
P	= 

##  =()<CC	= @<CC>@>()=
CC	= gcc
##  =()<DEFS	= @<DEFS>@>()=
DEFS	= -I../include
##  =()<PERLINC = @<PERL_INC>@>()=
PERLINC = 
##  =()<CFLAGS	= @<CFLAGS>@ $(PERLINC)>()=
CFLAGS	= $(DEFS) -g $(PERLINC)
##  =()<DBZCFLAGS	= @<DBZCFLAGS>@>()=
DBZCFLAGS	= $(CFLAGS)
##  =()<LDFLAGS	= @<LDFLAGS>@>()=
LDFLAGS	= 
##  =()<LINTLIBSTYLE	= @<LINTLIBSTYLE>@>()=
LINTLIBSTYLE	= NONE
##  =()<LINTFLAGS	= @<LINTFLAGS>@ $(PERLINC)>()=
LINTFLAGS	= -b -h -z $(DEFS) $(PERLINC)
##  =()<LINTFILTER	= @<LINTFILTER>@>()=
LINTFILTER	= | sed -n -f ../sedf.sun
##  =()<YACC	= @<YACC>@>()=
YACC	= yacc
##  =()<CTAGS		= @<CTAGS>@>()=
CTAGS		= ctags -t -w
##  =()<PROF	= @<PROF>@>()=
PROF	= -pg

##  =()<RANLIB	= @<RANLIB>@>()=
RANLIB	= ranlib

##  For OSx systems, get these from the ATT Universe libc.
##  See the rule for version.o, below.
OSXATTOBJ= version.o memchr.o memcmp.o memcpy.o memset.o strchr.o strrchr.o

##  =()<DBZDIR	= @<DBZDIR>@>()=
DBZDIR	= ../dbz
##  =()<MISSING_SRC = @<MISSING_SRC>@>()=
MISSING_SRC = 
##  =()<MISSING_OBJ = @<MISSING_OBJ>@>()=
MISSING_OBJ = 

SOURCES = $(MISSING_SRC) \
	checkart.c cleanfrom.c clientactive.c clientlib.c closeonexec.c \
	dbz.c defdist.c findheader.c genid.c getconfig.c getdtab.c \
	getfqdn.c getmodaddr.c gettime.c inndcomm.c innvers.c localopen.c \
	lockfile.c nonblocking.c parsedate.c perl.c qio.c radix32.c readin.c \
	remopen.c resource.c sendarticle.c sendpass.c tempname.c \
	waitnb.c wildmat.c \
	xfopena.c xmalloc.c xmemerr.c xrealloc.c xwrite.c xwritev.c
OBJECTS = $(MISSING_OBJ) \
	checkart.o cleanfrom.o clientactive.o clientlib.o closeonexec.o \
	dbz.o defdist.o findheader.o genid.o getconfig.o getdtab.o \
	getfqdn.o getmodaddr.o gettime.o inndcomm.o innvers.o localopen.o \
	lockfile.o nonblocking.o parsedate.o perl.o qio.o radix32.o readin.o \
	remopen.o resource.o sendarticle.o sendpass.o tempname.o \
	waitnb.o wildmat.o \
	xfopena.o xmalloc.o xmemerr.o xrealloc.o xwrite.o xwritev.o

all:			libinn.a

install:		../libinn.a ../llib-linn.ln

clobber clean:
	rm -f *.o libinn.a llib-linn.ln
	rm -f parsedate parsedate.c
	rm -f profiled libinn_p.a
	rm -f all install lint lint.all

tags ctags:	$(SOURCES)
	$(CTAGS) $(SOURCES) ../include/*.h

libinn.a:		$(P) $(OBJECTS)
	ar r $@ $(OBJECTS)
	$(RANLIB) libinn.a

llib-linn.ln:		lint
	-$(SHELL) ./makellib.sh $(LINTLIBSTYLE) "$(LINTFLAGS)" $(SOURCES)

lint:			$(P) $(OBJECTS)
	@rm -f lint
	-lint -u $(LINTFLAGS) $(SOURCES) $(LINTFILTER) >lint.all
	-grep -v yaccpar <lint.all >lint

##  Profiling.  The rules are a bit brute-force, but good enough.
profiled:		../libinn_p.a
	date >$@

../libinn_p.a:		$(SOURCES)
	rm -f $(OBJECTS)
	$(MAKE) libinn.a CFLAGS="$(CFLAGS) $(PROF)"
	mv libinn.a ../libinn_p.a
	$(RANLIB) ../libinn_p.a
	rm -f $(OBJECTS)

parsedate.c:		parsedate.y
	@echo Expect 6 shift/reduce conflicts
	$(YACC) parsedate.y
	@mv y.tab.c parsedate.c

parsedate:		$(P) parsedate.c gettime.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ -DTEST -DYYDEBUG parsedate.c gettime.o

##  These rules will only be triggered if syslog appears in the MISSING_xxx
##  macros above.
syslog.o:		syslog.c
syslog.c:		../syslog/syslog.c
	(cd .. ; $(MAKE) syslogfix )

## This rule will only be triggered if $(OSXATTOBJ) is in MISSING_OBJ.
version.o:
	@rm -f $(OSXATTOBJ)
	/.attbin/ar vx /usr/.attlib/libc.a $(OSXATTOBJ)

dbz.o:			dbz.c
	$(CC) $(DBZCFLAGS) -c dbz.c

../include/dbz.h:	$(DBZDIR)/dbz.h
	@rm -f ../include/dbz.h
	cp $(DBZDIR)/dbz.h ../include/dbz.h

dbz.c:			$(DBZDIR)/dbz.c
	cp $(DBZDIR)/dbz.c dbz.c

##  Only do this if you're building a release kit.
PATCH:			dbz.pch.intro
	rcs -l dbz.pch
	@rm -f dbz.pch
	cat dbz.pch.intro >dbz.pch
	@echo "Ignore diff's exit status."
	-diff -c $(DBZDIR)/dbz.c dbz.c >>dbz.pch
	ci -u dbz.pch

##
ccenter:	$(SOURCES)
	#load $(CFLAGS) $(SOURCES)

##  Low-level install actions.
../libinn.a:		libinn.a
	@rm -f $@
	cp libinn.a $@
	$(RANLIB) $@
../llib-linn.ln:	llib-linn.ln
	@rm -f $@
	cp llib-linn.ln $@

##  Dependencies.  Default list, below, is probably good enough.
depend:		Makefile $(SOURCES) ../include/dbz.h
	makedepend $(DEFS) $(SOURCES)

# DO NOT DELETE THIS LINE -- make depend depends on it.

checkart.o: ../include/clibrary.h
checkart.o: ../include/configdata.h
checkart.o: ../include/configdata.h
checkart.o: ../include/nntp.h
cleanfrom.o: ../include/clibrary.h
cleanfrom.o: ../include/configdata.h
cleanfrom.o: ../include/configdata.h
cleanfrom.o: ../include/macros.h
cleanfrom.o: ../include/macros.h
clientactive.o: ../include/clibrary.h
clientactive.o: ../include/configdata.h
clientactive.o: ../include/configdata.h
clientactive.o: ../include/libinn.h
clientactive.o: ../include/macros.h
clientactive.o: ../include/macros.h
clientactive.o: ../include/nntp.h
clientactive.o: ../include/paths.h
clientactive.o: ../include/paths.h
clientlib.o: ../include/clibrary.h
clientlib.o: ../include/configdata.h
clientlib.o: ../include/configdata.h
clientlib.o: ../include/libinn.h
clientlib.o: ../include/myserver.h
clientlib.o: ../include/nntp.h
clientlib.o: ../include/paths.h
clientlib.o: ../include/paths.h
closeonexec.o: ../include/clibrary.h
closeonexec.o: ../include/configdata.h
closeonexec.o: ../include/configdata.h
dbz.o: ../include/dbz.h
defdist.o: ../include/clibrary.h
defdist.o: ../include/configdata.h
defdist.o: ../include/configdata.h
defdist.o: ../include/libinn.h
defdist.o: ../include/macros.h
defdist.o: ../include/paths.h
defdist.o: ../include/paths.h
findheader.o: ../include/clibrary.h
findheader.o: ../include/configdata.h
findheader.o: ../include/configdata.h
findheader.o: ../include/libinn.h
findheader.o: ../include/libinn.h
findheader.o: ../include/macros.h
genid.o: ../include/clibrary.h
genid.o: ../include/configdata.h
genid.o: ../include/configdata.h
genid.o: ../include/libinn.h
getconfig.o: ../include/clibrary.h
getconfig.o: ../include/configdata.h
getconfig.o: ../include/configdata.h
getconfig.o: ../include/libinn.h
getconfig.o: ../include/libinn.h
getconfig.o: ../include/macros.h
getconfig.o: ../include/paths.h
getdtab.o: ../include/clibrary.h
getdtab.o: ../include/configdata.h
getdtab.o: ../include/configdata.h
getfqdn.o: ../include/clibrary.h
getfqdn.o: ../include/configdata.h
getfqdn.o: ../include/configdata.h
getfqdn.o: ../include/libinn.h
getfqdn.o: ../include/libinn.h
getfqdn.o: ../include/paths.h
getmodaddr.o: ../include/clibrary.h
getmodaddr.o: ../include/configdata.h
getmodaddr.o: ../include/configdata.h
getmodaddr.o: ../include/libinn.h
getmodaddr.o: ../include/macros.h
getmodaddr.o: ../include/nntp.h
getmodaddr.o: ../include/nntp.h
getmodaddr.o: ../include/paths.h
getmodaddr.o: ../include/paths.h
gettime.o: ../include/clibrary.h
gettime.o: ../include/configdata.h
gettime.o: ../include/configdata.h
gettime.o: ../include/libinn.h
inndcomm.o: ../include/clibrary.h
inndcomm.o: ../include/configdata.h
inndcomm.o: ../include/configdata.h
inndcomm.o: ../include/inndcomm.h
inndcomm.o: ../include/libinn.h
inndcomm.o: ../include/macros.h
inndcomm.o: ../include/macros.h
inndcomm.o: ../include/nntp.h
inndcomm.o: ../include/paths.h
inndcomm.o: ../include/paths.h
innvers.o: ../include/clibrary.h
innvers.o: ../include/configdata.h
innvers.o: ../include/configdata.h
innvers.o: ../include/patchlevel.h
innvers.o: ../include/patchlevel.h
localopen.o: ../include/clibrary.h
localopen.o: ../include/configdata.h
localopen.o: ../include/configdata.h
localopen.o: ../include/macros.h
localopen.o: ../include/nntp.h
localopen.o: ../include/nntp.h
localopen.o: ../include/paths.h
lockfile.o: ../include/clibrary.h
lockfile.o: ../include/configdata.h
lockfile.o: ../include/configdata.h
nonblocking.o: ../include/clibrary.h
nonblocking.o: ../include/configdata.h
nonblocking.o: ../include/configdata.h
parsedate.o: ../include/clibrary.h
parsedate.o: ../include/configdata.h
parsedate.o: ../include/configdata.h
parsedate.o: ../include/libinn.h
parsedate.o: ../include/libinn.h
parsedate.o: ../include/macros.h
perl.o: ../include/clibrary.h
perl.o: ../include/configdata.h
perl.o: ../include/configdata.h
perl.o: ../include/logging.h
qio.o: ../include/clibrary.h
qio.o: ../include/configdata.h
qio.o: ../include/configdata.h
qio.o: ../include/libinn.h
qio.o: ../include/macros.h
qio.o: ../include/qio.h
qio.o: ../include/qio.h
radix32.o: ../include/clibrary.h
radix32.o: ../include/configdata.h
radix32.o: ../include/configdata.h
readin.o: ../include/clibrary.h
readin.o: ../include/configdata.h
readin.o: ../include/configdata.h
readin.o: ../include/libinn.h
readin.o: ../include/macros.h
readin.o: ../include/macros.h
remopen.o: ../include/clibrary.h
remopen.o: ../include/configdata.h
remopen.o: ../include/configdata.h
remopen.o: ../include/libinn.h
remopen.o: ../include/nntp.h
remopen.o: ../include/paths.h
remopen.o: ../include/paths.h
resource.o: ../include/clibrary.h
resource.o: ../include/configdata.h
resource.o: ../include/configdata.h
resource.o: ../include/macros.h
sendarticle.o: ../include/clibrary.h
sendarticle.o: ../include/configdata.h
sendarticle.o: ../include/configdata.h
sendarticle.o: ../include/libinn.h
sendarticle.o: ../include/libinn.h
sendarticle.o: ../include/nntp.h
sendpass.o: ../include/clibrary.h
sendpass.o: ../include/configdata.h
sendpass.o: ../include/configdata.h
sendpass.o: ../include/libinn.h
sendpass.o: ../include/macros.h
sendpass.o: ../include/nntp.h
sendpass.o: ../include/paths.h
sendpass.o: ../include/paths.h
tempname.o: ../include/clibrary.h
tempname.o: ../include/configdata.h
tempname.o: ../include/configdata.h
waitnb.o: ../include/clibrary.h
waitnb.o: ../include/configdata.h
waitnb.o: ../include/configdata.h
wildmat.o: ../include/clibrary.h
wildmat.o: ../include/configdata.h
wildmat.o: ../include/configdata.h
xfopena.o: ../include/clibrary.h
xfopena.o: ../include/configdata.h
xfopena.o: ../include/configdata.h
xmalloc.o: ../include/clibrary.h
xmalloc.o: ../include/configdata.h
xmalloc.o: ../include/configdata.h
xmalloc.o: ../include/libinn.h
xmalloc.o: ../include/macros.h
xmalloc.o: ../include/macros.h
xmemerr.o: ../include/clibrary.h
xmemerr.o: ../include/configdata.h
xmemerr.o: ../include/configdata.h
xrealloc.o: ../include/clibrary.h
xrealloc.o: ../include/configdata.h
xrealloc.o: ../include/configdata.h
xrealloc.o: ../include/libinn.h
xrealloc.o: ../include/macros.h
xrealloc.o: ../include/macros.h
xwrite.o: ../include/clibrary.h
xwrite.o: ../include/configdata.h
xwrite.o: ../include/configdata.h
xwritev.o: ../include/clibrary.h
xwritev.o: ../include/configdata.h
xwritev.o: ../include/configdata.h
