#! /bin/sh
##  $Revision: 1.34 $
##  Daily news maintenance.
##  Optional arguments:
##	expdir=xxx	Directory in which to build new history file
##	expirectl=xxx	Use xxx as expire.ctl file
##	flags=xxx	Pass xxx flags to expire
##	noexpire	Do not expire
##	noexplog	Do not log expire output
##	nologs		Do not scan logfiles
##	nomail		Do not capture and mail output
##	norenumber	Do not renumber the active file
##	norm		Do not remove certain old files
##	norotate	Do not rotate logfiles
##	nostat		Do not run innstat
##	notdaily        Not a daily run, and therefore implies nologs.
##	delayrm		Delay unlink files, then do it quicker (expire -z)
##	/full/path	Path to a program to run before expiring

##  =()<. @<_PATH_SHELLVARS>@>()=
. /var/news/etc/innshellvars

EXPLOG=${MOST_LOGS}/expire.log
INNSTAT=${NEWSBIN}/innstat
HOSTNAME=`hostname`
DATE=`date`

##  If your expire does not pause or throttle innd, enable this next line:
#MESSAGE="Expiration script $$"
##  Renumber all at once, or in steps?  Set to the delay if steps (that
##  may take a long time!).
RENUMBER=

PROGNAME=news.daily
LOCK=${LOCKS}/LOCK.${PROGNAME}

##  Set defaults.
DAILY=true
DOEXPIRE=true
DOEXPLOG=true
DOEXPIREOVER=false
DOLOGS=true
DOMAIL=true
DORENUMBER=true
DORM=true
DOSTAT=true
EXPIRECTL=
EXPDIR=
EXPIREFLAGS="-v1"
EXPIREOVERFLAGS=
PROGRAMS=
REASON=
RMFILE=
SCANARG=

##  Parse JCL.
for I
do
    case "X$I" in
    Xdelayrm)
	RMFILE=${MOST_LOGS}/expire.rm
	;;
    Xexpctl=*)
	EXPIRECTL=`expr "${I}" : 'expctl=\(.*\)'`
	case ${EXPIRECTL} in
	/*)
	    ;;
	*)
	    EXPIRECTL=`/bin/pwd`/${EXPIRECTL}
	    ;;
	esac
	;;
    Xexpdir=*)
	EXPDIR=`expr "${I}" : 'expdir=\(.*\)'`
	;;
    Xexpireover)
	DOEXPIREOVER=true
	;;
    Xexpireoverflags=*)
	EXPIREOVERFLAGS=`expr "${I}" : 'expireoverflags=\(.*\)'`
	;;
    Xflags=*)
	EXPIREFLAGS=`expr "${I}" : 'flags=\(.*\)'`
	;;
    Xnotdaily)
        DAILY=false
	DOLOGS=false
	;;
    Xnoexpire)
	DOEXPIRE=false
	;;
    Xnoexplog)
	DOEXPLOG=false
	;;
    Xnologs)
	DOLOGS=false
	;;
    Xnomail)
	DOMAIL=false
	MAIL=cat
	;;
    Xnonn)
	# Ignore this.
	;;
    Xnorenumber)
	DORENUMBER=false
	;;
    Xnorm)
	DORM=false
	;;
    Xnorotate)
	SCANARG="${SCANARG} norotate"
	;;
    Xnostat)
	DOSTAT=false
	;;
    X/*)
	PROGRAMS="${PROGRAMS} ${I}"
	;;
    *)
	echo "Unknown flag ${I}" 1>&2
	exit 1
	;;
    esac
done
##
## Setup mail subject
##
if ${DAILY} ; then
   MAILSUBJ="${HOSTNAME} daily usenet report for ${DATE}"
else
   MAILSUBJ="${HOSTNAME} intermediate usenet report for ${DATE}"
fi

MAIL="${MAILCMD} -s \"FAILED: ${MAILSUBJ}\" ${NEWSMASTER}"

${DOEXPIRE} || {
    EXPDIR=
    RMFILE=
}

test -n "${EXPDIR}" && {
	test -z "${REASON}" && REASON="Expiring $$ on ${EXPDIR}"
	EXPIREFLAGS="${EXPIREFLAGS} '-d${EXPDIR}' '-r${REASON}'"
}

test -n "${RMFILE}" && {
    EXPIREFLAGS="${EXPIREFLAGS} -z${RMFILE}"
    EXPIREOVERFLAGS="${EXPIREOVERFLAGS} -z ${RMFILE}.$$"
}
test -z "${EXPIREOVERFLAGS}" && EXPIREOVERFLAGS=-s

if ${DOMAIL} ; then
    ##  Try to get a temporary file.
    TEMP=${TMPDIR}/doex$$
    test -f ${TEMP} && {
        MAIL="${MAILCMD} -s \"${HOSTNAME}: Delayed USENET report\" ${NEWSMASTER}"
	echo "Temporary file ${TEMP} exists" | eval ${MAIL}
	exit 1
    }
    touch ${TEMP}
    chmod 0660 ${TEMP}
    exec 3>&1 >${TEMP} 2>&1
fi

cd ${NEWSLIB}

##  Show the status of the news system.
${DOSTAT} && {
    ${INNSTAT}
    echo ''
}

##  Lock out others.
trap 'rm -f ${LOCK} ; exit 1' 1 2 3 15
shlock -p $$ -f ${LOCK} || {
    ( echo "$0: Locked by `cat ${LOCK}`"; ${INNSTAT} ) | eval ${MAIL}
    exit 1
}

##  Run any user programs.
if [ -n "${PROGRAMS}" ] ; then
    for P in ${PROGRAMS} ; do
	echo ''
	echo "${P}:"
	eval ${P}
    done
fi

##  The heart of the matter:  prologs, expire, epilogs.
if ${DOEXPIRE} ; then

    ## Wait to be fairly certain innwatch is not in the middle of a pass
    ## Since we're locked, innwatch will pause now till we're done
    sleep 30

    ##  See if we're throttled for lack of space.
    SERVERMODE=`ctlinnd mode 2>/dev/null | ${SED} 1q`
    case "${SERVERMODE}" in
    'Server paused'*'[innwatch:'*)
	## If paused, by innwatch, then turn pause into throttle
	## as we're going to stay that way for a while now
	ctlinnd -s throttle "`expr \"${SERVERMODE}\" : 'Server paused \(.*\)'`"
    esac
    case "${SERVERMODE}" in
    *space*" -- throttling")
	echo "${SERVERMODE} -- trying to recover"
	THROTTLED=true
	EXPIREFLAGS="${EXPIREFLAGS} -n"
	MESSAGE=
	;;
    *"[innwatch:"*)
	echo "${SERVERMODE} -- pressing on"
	THROTTLED=false
	EXPIREFLAGS="${EXPIREFLAGS} -n"
	MESSAGE=
	DORENUMBER=false
	;;
    *)
	THROTTLED=false
	;;
    esac

    ##  Throttle server if we need to.
    if [ -n "${MESSAGE}" ] ; then
	ctlinnd -s -t120 throttle "${MESSAGE}" 2>&1 || {
	    ( echo "$0: Cannot throttle news"; ${INNSTAT} ) | eval ${MAIL}
	    exit 1
	}
    fi

    #
    #  Get rid of an old expire RMFILE since news.daily locks itself and
    #  we would not get here if another instance were still running.
    #        
    if [ -n "${RMFILE}" ] ; then
        rm -f ${RMFILE}
    fi

    ##  Actual expire the articles (finally!).
    test -n "${EXPIRECTL}" && EXPIREFLAGS="${EXPIREFLAGS} ${EXPIRECTL}"
    if ${DOEXPLOG}; then
	echo "expire begin `date`: (${EXPIREFLAGS})"		>>${EXPLOG}
	( exec 2>&1 ; eval expire "${EXPIREFLAGS}" ) \
	    | ${SED} -e 's/^/    /' >>${EXPLOG}
	echo "expire end `date`"				>>${EXPLOG}
    else
	eval expire "${EXPIREFLAGS}"
    fi

    ##  If built on another filesystem, move history files.
    if [ -n "${EXPDIR}" ] ; then
	if [ ! -f ${EXPDIR}/history.n -o ! -f ${EXPDIR}/history.n.done ] ; then
	    ( echo "$0: No new history files"; ${INNSTAT} ) | eval ${MAIL}
	    exit 1
	fi
	mv -f ${EXPDIR}/history.n ${HISTORY}
	mv -f ${EXPDIR}/history.n.dir ${HISTORY}.dir
	mv -f ${EXPDIR}/history.n.pag ${HISTORY}.pag
	rm -f ${EXPDIR}/history.n.done
	case "${EXPIREFLAGS}" in
	*-n*)
	    ;;
	*)
	    MESSAGE="${REASON}"
	    ;;
	esac
    fi

    ##  Restart the server if we need to.
    if ${THROTTLED} || test -n "${MESSAGE}" ; then
	ctlinnd -s go "${MESSAGE}" 2>&1 || {
	    ( echo "$0: Cannot unthrottle news"; ${INNSTAT} ) | eval ${MAIL}
	    exit 1
	}
    fi
fi


${DOLOGS} && {
    echo ''
    scanlogs ${SCANARG}
}

##  Remove old sockets.
${DORM} &&
    find ${TEMPSOCKDIR} -name "${TEMPSOCK}" -mtime +2 -exec rm -f '{}' ';'

##  Did we became throttled during the run?
SERVERMODE=`ctlinnd mode 2>/dev/null | ${SED} 1q`
case "${SERVERMODE}" in
*space*" -- throttling")
    ##  We did, try to unthrottle the server.
    echo "${SERVERMODE} -- trying to recover"    
    ctlinnd -s go ""
    ;;
esac

##  Show the status of the news system after expiration.
${DOSTAT} && {
    echo 'Post expiration status:' ; echo ''
    ${INNSTAT}
    echo ''
}

##  Release the lock now, everything below this must be able to withstand
##  simultaneous news.daily's running.   We do this so innwatch can start
##  monitoring again asap after the expire is done -- removing the
##  articles isn't critical, nor is the renumber.
rm ${LOCK}

## Remove the articles that are supposed to go
if ${DOEXPIRE} ; then
    test -n "${RMFILE}" -a -s "${RMFILE}" && {
	mv ${RMFILE} ${RMFILE}.$$ && RMFILE=${RMFILE}.$$
	test -n "${TMPDIR}" && T="-T ${TMPDIR}"
	sort -u -o ${RMFILE} ${RMFILE}
	${DOEXPIREOVER} && {
	    eval expireover "${EXPIREOVERFLAGS}"
	    DOEXPIREOVER=false
	}
	expirerm ${RMFILE}
    }
    ${DOEXPIREOVER} && eval expireover "${EXPIREOVERFLAGS}"
fi

##  Renumber the active file.
if ${DORENUMBER} ; then
    echo ''
    echo 'Renumbering active file.'
    if [ -z "${RENUMBER}" ] ;then
	ctlinnd -s -t`wc -l <${ACTIVE}` renumber '' 2>&1
    else
	while read GROUP hi lo flag ; do
	    ctlinnd -s renumber ${GROUP} 2>&1
	    sleep ${RENUMBER}
	done <${ACTIVE}
    fi
fi

##  Mail the report.
if ${DOMAIL} ; then
    # Stop using the temp file, and mail captured output.
    exec 1>&3 2>&1 3>&-
    MAIL="${MAILCMD} -s \"${MAILSUBJ}\" ${NEWSMASTER}"
    test -s ${TEMP} && cat ${TEMP} | sed -e 's/^~/~~/' | eval ${MAIL}
    rm -f ${TEMP}
fi

##  All done
if ${DAILY} ; then
    date >${NEWSLIB}/.news.daily
fi
${RNEWS} -U
exit 0
