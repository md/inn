##  $Revision: 1.17 $
##  newsfeeds - determine where Usenet articles get sent
##  Format:
##	site[/exclude,exclude...]\
##		:pattern,pattern...[/distrib,distrib...]\
##		:flag,flag...\
##		:param
##  Summary of flags:
##	<size		Article must be less then size bytes.
##	Aitems		Article checks -- d (must have Distribution header)
##			p (don't check for site in Path header).
##	Bhigh/low	Internal buffer size before writing to output.
##	H[count]	Article must have less then count hops; default is 1.
##	Isize		Internal buffer size (if a file feed)
##	Nm		Only moderated groups that match the patterns.
##	Nu		Only unmoderated groups that match the patterns.
##	Ssize		Start spooling if more than size bytes get queued.
##	Ttype		Feed types -- f (file) m (funnel; param names the
##			real entry) p (pipe to program) c (send to stdin
##			channel of param's sub-process); x (like c, but
##			handles commands on stdin).
##	Witems		What to write -- b (article bytesize) f (full path)
##			g (first newsgroup) m (Message-ID) n (relative
##			path) s (site that fed article) t (time received)
##			* (names of funnel feed-in's or all sites that get
##			the article) N (Newsgroups header) D (Distribution
##			header) H (all headers) O (overview data) R
##			(replication data).
##  Param field depends on T flag.  For Tf, relative paths are from the
##  out.going directory.  For Tp and Tc, it is a shell command to execute.
##  If a Tm refers to this entry (which will have its own T param) then "*"
##  is expanded to all the funnel sites that triggered this one.  Useful
##  for spawning one mail process, e.g.
##
##  This file is complicated -- see newsfeeds.5!

##  This is the local site.
##  The "pattern" field gives the intial subscription list for
##  all other sites.  You might want to put "!control,!junk,!<local>.*"
##  there.  The "distrib" subfield limits incoming articles.
##
##  You can also have ME/bad.site: to refuse articles from a particular
##  site (by matching the Path: entry).  Other pseudo-sites may be put
##  in here, to REFUSE certain types of 3rd-party cancel messages
##  (See the "Cancel FAQ" news.admin.net-abuse.misc):
##	cyberspam	Spam cancels, munged articles, binary postings
##	spewcancel	just munged articles from runaway gateways
##	bincancel	just binary postings to non-binaries groups
##
##  Note that refusing articles means you won't offer them to sites you feed

## Default of  everything to everybody except for junk, control, anything
## with "local" as the newgroup prefix (i.e. matches "localhost.stuff") or
## groups under foo. Articles posted to any group under alt.binaries.warez
## will not get propogated, even if they're cross posted to something that
## is.
ME\
	:*,@alt.binaries.warez.*,!junk,!control*,!local*,!foo.*\
		/world,usa,na,gnu,bionet,pubnet,u3b,eunet,vmsnet,inet,ddn,k12\
	::

## Create the links for cross posted articles
#crosspost:*:Tc,Ap,WR:/usr/news/bin/crosspost

# Feed all moderated source postings to an archiver
#source-archive!:!*,*sources*,!*wanted*,!*.d\
#	:Tc,Wn:/usr/news/bin/archive -f -i /usr/spool/news.archive/INDEX

##  News overview
#overview!:*:Tc,WO:/usr/news/bin/overchan

# Feed all local non-internal postings to nearnet; sent off-line via
# nntpsend or send-nntp.
#nic.near.net\
#	:!junk/!foo\
#	:Tf,Wnm:nic.near.net

# A real-time nntplink feed
#uunet\
#	:/!foo\
#	:Tc,Wnm:/usr/news/bin/nntplink -i stdin news.uu.net

# Capture all Foo, Incorporated, postings
#capture\
#	:*/foo\
#	:Tp,H2:/usr/news/local/capture %s

# A UUCP feed, where we try to keep the "batching" between 4 and 1K.
#ihnp4\
#	:!junk,!control/!foo\
#	:Tf,Wnb,B4096/1024:
